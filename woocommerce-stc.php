<?php
/**
 * Plugin Name: Custom price for weight
 * Description: Add custom price to cart and checkout on the basis of weight
 * Version: 1.0.0
 * Author: Nadeem
 * Author URI: https://bitbucket.org/stclub/woocommerce-stc
 * Text Domain: stc
*/

// Exit if accessed directly
	if ( ! defined( 'ABSPATH' ) ) {
	 exit;
	}
	function add_js_scripts() {
	  
	  wp_enqueue_script( 'script', plugin_dir_url( __DIR__ ). 'woocommerce-stc/script.js', array ( 'jquery' ), 1.1, true);
	}
	add_action( 'wp_enqueue_scripts', 'add_js_scripts' );
	// create custom plugin settings menu
	add_action('admin_menu', 'price_plugin_create_menu');

	function price_plugin_create_menu() {

		//create new top-level menu
		add_menu_page('Price Plugin Settings', 'Price Settings', 'administrator', __FILE__, 'price_settings_page' , plugins_url('/images/icon.png', __FILE__) );
	}

	function price_settings_page() {
		if(isset($_POST['submit'])) {
			$data = array();

			$data['f_c_amount'] = sanitize_text_field($_POST['f_c_amount']);
			$data['s_c_amount'] = sanitize_text_field($_POST['s_c_amount']);
			$data['t_c_amount'] = sanitize_text_field($_POST['t_c_amount']);
			$data['f_c_weight'] = sanitize_text_field($_POST['f_c_weight']);
			$data['s_c_weight'] = sanitize_text_field($_POST['s_c_weight']);
			$data['t_c_weight'] = sanitize_text_field($_POST['t_c_weight']);

			update_option( 'custom_price_data', $data );
		}
	?>
	<div class="wrap">
	<h1>Add custom Price</h1>
	<?php
	    $p_c_data = array();
	    $c_p_data['f_c_amount'] = 0;
		$c_p_data['s_c_amount'] = 0;
		$c_p_data['t_c_amount'] = 0;
		$c_p_data['f_c_weight'] = 0;
		$c_p_data['s_c_weight'] = 0;
		$c_p_data['t_c_weight'] = 0;
		if(false !== get_option('custom_price_data')) {
			$c_p_data = get_option('custom_price_data');	
		}
	    

	?>
	<form method="post" action="">
	    
	    <table class="form-table">
	        <tr valign="top">
	        <th scope="row">Add Custom Amount</th>
	        <td><input type="text" name="f_c_amount" value="<?php echo $c_p_data['f_c_amount']; ?>" /></td>
	        <th scope="row">If Product Weight >= </th>
	        <td><input type="text" name="f_c_weight" value="<?php echo $c_p_data['f_c_weight']; ?>" /></td>
	        </tr>
	         
	        <tr valign="top">
	        <th scope="row">Add Custom Amount</th>
	        <td><input type="text" name="s_c_amount" value="<?php echo $c_p_data['s_c_amount']; ?>" /></td>
	        <th scope="row">If Product Weight >= </th>
	        <td><input type="text" name="s_c_weight" value="<?php echo $c_p_data['s_c_weight']; ?>" /></td>
	        </tr>
	        
	        <tr valign="top">
	        <th scope="row">Add Custom Amount</th>
	        <td><input type="text" name="t_c_amount" value="<?php echo $c_p_data['t_c_amount']; ?>" /></td>
	        <th scope="row">If Product Weight >= </th>
	        <td><input type="text" name="t_c_weight" value="<?php echo $c_p_data['t_c_weight']; ?>" /></td>
	        </tr>
	    </table>
	    
	    <?php submit_button(); ?>

	</form>
	</div>
	<?php } 

	add_action( 'woocommerce_cart_calculate_fees','custom_pack_fee', 10 , 1 );
	function custom_pack_fee( $cart_object ) {
	    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
	        return;
	           
	 global $woocommerce;
	 $amount = WC()->cart->get_cart_contents_total();

	 $total_weight = $woocommerce->cart->cart_contents_weight;

	 if(false !== get_option('custom_price_data')):
	 $data = get_option('custom_price_data');

	 if ( !WC()->cart->is_empty() ):
	    if($total_weight >= $data['f_c_weight'] && $total_weight < $data['s_c_weight']){
	    	$cart_object->add_fee( __('Custom Price', 'woocommerce'), ($data['f_c_amount']), true );
	    }elseif ($total_weight >= $data['s_c_weight'] && $total_weight < $data['t_c_weight']) {
	    	
	    	
	    	$cart_object->add_fee( __('Custom Price', 'woocommerce'), ($data['s_c_amount']), true );
	    }elseif ($total_weight >= $data['t_c_weight']) {
	    	 
	    	$cart_object->add_fee( __('Custom Price', 'woocommerce'), ($data['t_c_amount']), true );
	    	
	    }else {
	    	//
	    }

	    endif;
	    
	endif;
	}
	function total_price1($cart_object) {
	 if ( is_admin() && ! defined( 'DOING_AJAX' ) )
	        return;
		global $woocommerce;
	 $amount = WC()->cart->get_cart_contents_total();

	 $total_weight = $woocommerce->cart->cart_contents_weight;

	 if(false !== get_option('custom_price_data')):
	 $data = get_option('custom_price_data');

	 if ( !WC()->cart->is_empty() ):
	    if($total_weight >= $data['f_c_weight'] && $total_weight < $data['s_c_weight']){
	    	
	    	$cart_object->total = $amount + $data['f_c_amount'];
	    	
	    }elseif ($total_weight >= $data['s_c_weight'] && $total_weight < $data['t_c_weight']) {
	    	
	    	$cart_object->total = $amount + $data['s_c_amount'];
	    	
	    }elseif ($total_weight >= $data['t_c_weight']) {
	    	 
	    	$cart_object->total = $amount + $data['t_c_amount'];
	    	
	    }else {
	    	$cart_object->total = $amount;
	    }
	      
	       //  $cart_object->subtotal 

	    endif;
	    
	endif;
	}
	add_filter('woocommerce_after_calculate_totals', 'total_price1');